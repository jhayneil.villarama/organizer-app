import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
})
export class LoadingPage implements OnInit {
  userInput;
  constructor(private router: Router, public navCtrl: NavController) { }

  ngOnInit() {
  }

  logMeIn() {
    this.router.navigate(['/home']);
    alert(this.userInput)
    console.log(this.userInput)
  }
}
