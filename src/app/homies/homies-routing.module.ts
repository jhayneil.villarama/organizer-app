import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomiesPage } from './homies.page';

const routes: Routes = [
  {
    path: '',
    component: HomiesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomiesPageRoutingModule {}
