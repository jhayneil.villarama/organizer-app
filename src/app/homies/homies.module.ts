import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomiesPageRoutingModule } from './homies-routing.module';

import { HomiesPage } from './homies.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomiesPageRoutingModule
  ],
  declarations: [HomiesPage]
})
export class HomiesPageModule {}
