import { Component, OnInit } from '@angular/core';
import { ElementRef, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
declare var google: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  map: any;
  @ViewChild('map', {read: ElementRef, static: false}) mapRef: ElementRef;

  infoWindows: any = [];
  markers: any =  [
    {
      title: "Batangas City",
      latitude: "13.7565",
      longitude: "121.0583"
    },
    {
      title: "Alangilan",
      latitude: "13.7819",
      longitude: "121.0681"
    },
    {
      title: "Libjo",
      latitude: "13.7351",
      longitude: "121.06502"
    },
    {
      title: "Kumintang Ibaba",
      latitude: "13.7659",
      longitude: "121.0650"
    },
    {
      title: "Calicanto",
      latitude: "13.7665",
      longitude: "121.0523"
    },
    {
      title: "Cuta",
      latitude: "13.7455",
      longitude: "121.0550"
    }
  ]

  constructor(private router: Router) { }

  ionViewDidEnter(){
  this.showMap();
  }

  addMarkersToMap(markers) {
    for(let marker of markers) {
      let position = new google.maps.LatLng(marker.latitude, marker.longitude);
      let mapMarker = new google.maps.Marker({
        position: position,
        title: marker.title,
        latitude: marker.latitude,
        longitude: marker.longitude
      });

      mapMarker.setMap(this.map);
      this.addInfoWindowToMarker(mapMarker);
    }
  }

  addInfoWindowToMarker(marker) {

    let infoWindowContent = '<div id="content">'+
                            '<h2 id="firstheading" class"firstheading">' + marker.title + '</h2>' +
                            '<p>Latitude: ' + marker.latitude + '</p>' +
                            '<p>Longitude: ' + marker.longitude + '</p>' +
                            '</div>';

  let infoWindow = new google.maps.infoWindow({
    content: infoWindowContent
  });

  marker.addListener('click', () => {
    this.closeAllInfoWindows();
    infoWindow.open(this.map, marker);
  });
  this.infoWindows.push(infoWindow);
  }
  closeAllInfoWindows(){
    for(let window of this.infoWindows){
      window.close();
    }
  }
  showMap () {
    const location = new google.maps.LatLng(-17.824858, 31.053028);
    const options = {
      center: location,
      zoom: 15,
      disableDefaultUI: true
    }
    this.map = new google.maps.Map(this.mapRef.nativeElement, options);
    this.addMarkersToMap(this.markers);
  }

  ngOnInit() {
  }
  letMeHome(){
    this.router.navigate(['/home']);
  }

}
